---
layout: post
title:  "The crocs and the hooks"
date:   2019-05-27 00:00:00 -0300
categories: story
---

When the blue forest meets the sea, it isn’t abrupt like crossing the border or stepping over a cliff. There is a long stretch neither sea nor completely forest. It goes from a couple of puddles to tiny sand Islands. We called it the **Death Swamp**. Not the nicest name but a fitting one. It’s not a very fine placeafter all.

The first inhabitant you will notice is the **Aedes Abopictus** also known as the tiger mosquito. Named for its white stripes or maybe because after one day in the swamp some would prefer to face a tiger instead of their flying counterparts.

The second likely encounter is the crocodile. Often invisible until it’s too late. With impervious carapace, lightning reflexes and way too many teeth. They are king of the swamp for no other animal can challenge them.

Fortunately, they are lazy creatures and may let you pass by if they’re not hungry or too bored.

When they aren’t hunting nor chewing, they usually bicker with one another about the best puddle or a piece of prey. Bickering for a crocodile involves a lot of water splashing, tail slaps, a couple of well-placed bites, a surprising amount of blood and sometimes a lost limp or two. Try to avoid any participation.

But the biggest the fiercest, the most wicked, is an albino monster, far too clever. Her name is **Pearl**. The white queen. She lost her leg to a white shark when she was younger. She is the most dangerous of them all.

If the reptiles rule the swamp, the voodoo pirates are master on land. Sailing or paddling through the swamp on precarious barks, only they, are crazy enough to live there.
I am sorry to say that they are not nicer than the crocodiles. What they lack in teeth they make up for it in craziness. Oblivious to logic and self preservation. They bleach their face and blackened their teeth. They wear crocodile teeth necklaces. They are ferocious sword dancers and powerful magicians.

To prove the worth, the warriors must wrestle crocodile with bare hands. Then they replace the lost limbs with metal hooks and other the contraptions.

The magicians have potion to see the neither-realm and song, *Ahal puch..* to which the dead come back to dance. The most famous is an old crone with a face of an angel. She answers to **Xibalba** the red hair witch.
