---
layout: post
title:  "The witch with no name"
date:   2020-09-26 00:00:00 -0300
categories: story
---

Once, there was a little girl. Her father, she never knew, but she was every thing to her mother. They were poor but they were rich. Those were the days of laughing and dancing.

Her mother took ill. First a cough, then a faint, then a lot of time in bed. It ended like it began, with a last bloody cough. Those were the days of tears.
On the fresh mount of her mothers' grave. The little girl raked her arms with despair. Attracted by the blood and the tears a shadow came and licked her wounds. The girl spoke to the shadow and the shadow answered. A bargain was made, some blood for the courage to survive.

Indeed, the pale girl survived but life wasn't easy for an orphan girl. She got beat, whipped and soiled. Alone in the mud, she called the shadow and the shadow answered. Another bargain was made. Her pretty face for the strength to fight back. And as her sadness recess, her anger grew.

But life as a woman is never easy. Always she found stronger, bigger, crueler. She traded her eyes for dark power of revenge. Those were angry days.

The blind witch became known and feared. But she wanted more power. She perfected the dark art and what she couldn't learn, she traded for.
She traded her body for invincibility. Dressed in a black cloud she traded her soul for immortality.

At last, she had only one thing left to trade. The last gift from her mother. Her name, she gave away. She melded with the shadow and the shadow melded with her. Now are the days of terror. For the witch with no name rules the night and lurk in every shadow.
