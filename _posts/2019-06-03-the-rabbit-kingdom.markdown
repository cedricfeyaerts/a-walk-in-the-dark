---
layout: post
title:  "The rabbit kindom"
date:   2019-06-03 00:00:00 -0300
categories: story
---

Their fur varies from dark brown to snow white (which is the purest white) passing by cream, spotted, gray, grey, smoky brown, anthracite, nut, chocolate, ochre and peanut butter. 
Recognizable by their long ears and fluffy tail, the rabbit is polite if a bit frantic.

Country side rabbits are often timid and rightfully so. Usually more isolated, their neighbours includes snakes, foxes and wolves with whom they aren’t in the best of terms, to say the least. Never mind how friendly you might be, it is hard to stay neighborly when someone is trying to eat you.
This explain the rabbit first reflex (when surprised) is to jump in the next available hole. It explain also why they tend to organize into small communities. Those farming cooperatives are the cornerstone their society.

With an economy based on carrots and clover, farming is an honorable occupation. The carrot, once just a source of nutriment, has, nowadays, a quasi holy status. With hundred of varieties, such as *Cosmic purple, Merida, White satin, Solar yellow, Atomic red, Yoya, Parano, Touchon, Imperator...* 
The carrot connoisseur is a renowned profession. In some remote area, the animistic cult of the carrot god is still honored.

Since a couple hundred years, the different rabbit clan are now unified under the same banner (a gold carrot on a field of clover) and ruled by a king, currently Leonard the 23rd, or Lenny the Fluffy. His name due to his generous corpulence rather than the quality of is fur. He lives in a sumptuous underground palace at the very heart of Bunny City. 
 
Bunny city, like any other rabbit city is a maze of well maintained tunnels interconnecting privates and publics lair. Despite being underground, it is very clean, well maintained, with wooden panels, very good indication and public lighting.

All in all the society is supported by a strong administration. The population tend to be organized and respectful of the law. The aristocracy has privilege of art and academics but no one seems to mind this slight injustice.

If the rabbits are welcoming and open minded by nature. Some professions are suffer of ill repute. Adventurer among them isn’t understood. Those in the business are often outcast or even banished.

Hazee, is such an unfortunate. Born with a rare complexion, his fur is gentle pink. He was destined to an unusual life.