---
layout: post
title:  "The happy dwarves"
date:   2019-05-21 15:32:14 -0300
categories: story
---

At the very base of the mountain, when trees give up to the tall grass, live a couple of dwarves in a small wooden house. Some say they are 7 of them, some say 18. In any case there is too many for that little house.

When they are home, the stove in on and the valley smell of cookie or cheese for cooking is their weakness and passion.

When the sun comes down, their songs rise, high and strong. They are a joyful bunch. They sing strange songs. Some old ones, some made as they go. It doesn’t always rime but they re always loud and true.

When they aren’t home, they are “working” in the mountain. And by working they mean something very different from you and me. Maybe hunting some white goblins, braving the traps and danger of an ancient ruin, pillaging an abandoned mine or rescuing a little child lost in the maze beneath the mountain. They said they rescue the princess once but to them, every girl is a princess, every guest is a king.

Down, down they go with a smile and a torch. Those underground adventures are their game and the tunnel beneath the mountain, their playground.

Deeper and deeper they go. Then, suddenly, they freeze as one. They arch their ears, wrinkle their noses. They frown with worry.

Because down below there is such a thing even a mighty dwarf should fear.
