---
layout: post
title:  "The buckheads"
date:   2019-05-23 00:00:00 -0300
categories: story
---

The plain is windy and very dry. There isn’t much grass between the rocks in the pebbles. This is **the rusty plain**. It may seem a lonely place but it is far from empty.

Where do those strange metal formation come from? Hollow boulders peppering the gray with the red ochre? A long-forgotten heritage from the elder? A gift from a visitor from the stars? The common explanation, among the intelligentsia, is that of a natural phenomenon. A peculiar case of mineral crystallization due to the very specific combination of wind and metallic dust.

Naturally, the fauna adapted to this strange area. Hence the critters.

The critters roam the plain. Mostly shy, they avoid humans contacts.  But if you stay long enough, first you would hear the call made of beeps and clicks.
Then carefully they would risk out of the hiding. Made of bits and bobs tiny moving parts, like machine with the soul, their light blinking with curiosity.

No two are the same. Size ranging from smaller than a mouse to… Well let’s see bigger.
For most they are not very smart but some can speak. **The buckheads**.
They mostly talk gibberish or complete nonsense. Their answers often more puzzling than the question they are asked. The few and far between sufficiently clever to have a conversation are quite civilized, well lettered, polite if not obsequious.

I should let a warning here. They are surprisingly strong and despite the gentle nature they can get angered. The ire is as hard to predict but not to be trifled with.
