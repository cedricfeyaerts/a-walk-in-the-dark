---
layout: post
title:  "The dragon island"
date:   2020-09-26 00:00:00 -0300
categories: story
---

Girt by mist and cruel wind, soared a single island from the black sea. All cliffs. No beach. No cove. Only treacherous currents. This peak is only approachable by air.

There lay the last colony of wild dragons. Various in shapes, size and color, they fish, they fly, they play, but mostly they rest on the bare rock where no plant could survive.

The size vary from not more than a horse to as long as a war Galleon (150 feet). Their hide is thick as rock imitating a type of mineral. Mostly gray brown or green but some have brighter colors. The most striking of them old is the gold dragon. Sparkling skin akin to pirite.

They mostly feed on fish, of which the consume large quantity, but they can also fast for a long period during which they seem to hibernate.

Against popular belief, dragons rarely attack human. They are not the monster common literature describe them. They are powerful and dangerous creature but also wise and cunning.
Current scientific opinion, agree that they share verbal communication. 

Their lifespan is another enigma, no case of natural death was registered.
