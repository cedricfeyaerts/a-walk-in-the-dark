---
layout: post
title:  "The princess, the sultan and the city"
date:   2019-06-03 00:00:00 -0300
categories: story
---

Once upon a time, there was a sultan. The richest prince, descendant of a long merchant dynasty. He had everything, palaces, harem, mystical pets, mountains of gold, uniq artifacts. He had nothing to prove, nothing to gain, nothing to live for.

As his diplomatic duties require, he one day visited a grey country far west. In that grey country, in a grey castle, on a grey afternoon he met the local royals. A king with a grey beard and queen with grey hair and a princess with a grey sorrow.
There, when the sultan who had it all, laid eyes on the royal child, something quite ordinary had world wide consequences.

*She sighed...*

And with one breath she dusted his dry heart. One exhale and his soul shivered.

Daisy, was her name for her pale skin and golden hair. The sultan with in hundred harem felt love for the first time. But he discovered loss and despair too as she didn’t love him back.

Back in his land he gathered his best architects, artistes of renown, philosophers, even a sorcerer. He told them to build a city. There on the west edge of his kindom at the root of the smoky mountain, the greatest city on earth. It should have the ivory of her skin, the gold of her hair and the azul of her eyes.

And for 7 year they build a city, the whitest, shiniest. High ivory tower with golden roof. A griffon garden. At its center a deep oasis with bluest water.

And for 7 years, the poor Sultan visited Daisy in her grey castle. He never brought any gift other than stories for afar and a ear to her sorrowed heart.
Little by little, where there was nothing grew trust. And from trust grew fondness. On the 7th year she asked:

– Your are the rich among the richest but unlike my suiters, you never brought anything from your travel. Oh dear Sultan, tell me why?   
– But I did. He answered. I brought it as far as I could. It is all there, behind the smoky mountains in the east.

Because she was curious, and because something else had grew from fondness, she follow him to the white city. When she first laid eyes upon it, she sighed again and kissed him. She never came back to her grey country.

For a time, the city shined among the world. Artists, poets, mystics, musicians, merchants, nobles, they all came. The city grew in size and fame. The sultan grew old and concern for his legacy.

– I want the city to last forever, he told his sorcerer.   
– People come and go but stone endure, he answered. I will cast some of your love in blue ruby. Pure and rare like your passion for her.

Later the Sultan died, a statue was build. People cried and mourned but the city continue to shine.

Then the princess died. Song were sung and ballad were told. The city wavered but did not dim.

But now the stone is gone. Like poisoned in its roots the tower are now veined. The oasis turn to mud. The once colorful merchants streets are now plagued by beggars and thieves.

But nothing is lost, they say. Who will return the blue ruby will restore its grandeur. Who will return it and rule forever.
