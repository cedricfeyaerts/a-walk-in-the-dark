---
layout: post
title:  "The true school of magic"
date:   2020-10-10 00:00:00 -0300
categories: story
---

On the east slope of Craggle Mountain sits one of the most peculiar cities, **The impossible city of true Magic**.

There, halfway between rock, ice and sky, was founded the very first school only dedicated to the study of the noblest art.
Before, magic was taught only by apprenticeship. It was a real novelty at the time. And spit upon by many wizards who typically despise novelty. But it caught on and became very famous very fast.

As things go in this world, it didn’t keep its monopole very long. Soon a second school was founded. You will never guess where.... Just on the other side of the very same street of the first one. Magiciens have a tendency toward competition.
Of course it wasn’t long before a third and a fourth came almost simultaneously, just two numbers down.

A couple of centuries later, every second building is a school of its own.
Some count only a couple of students in a decrepit board house. Some count hundreds of teachers plus staff in castles and mansions in different parts of town.

The competition to attract new students tends to make things a bit confusing. Especially concerning naming. Here are some example you can find on the prospectus distributed in neighbouring hostels:

* The school of true magic
* The true school of magic
* The real school of real magic
* The original school of magic 2
* The first true real school of truth about magic

But where competition truly borders absurd is in architecture. Let’s just say that magicien like towers and they take pride in defying physics.

There is the classical tower standing on its head, a tower looping around itself, an invisible tower, a transparent tower (not the same thing, think about it), a tower made of water, a moving tower, a talking tower, a tower made of dreams.... I let you imagine the disagreement it implies.

An interesting fact about magic buildings is they tend to change numbers or street names overnight. Some schools are harder to find than others and it’s not always on purpose. Some even became a myth.

For security reason it has been forbidden to perform magic in the streets. Fortunately for the tourists , the fact that street is only the ground provides many loopholes to that rule.

You would have understood there is as many school of magic as there are letters in a Bruthian* Alphabet. Schools open and close every day.
 
Here are the most commonly known:

* *In the name of magic* Base on the belief of true naming.
* *The alchemist guild*
* *The path of the necromant* They use what was ones dead and isn’t quite living yet
* *The immer mind* Meditation and power of the mind
* *The wicca school*
* *Magus universalis* Their library is the most impressive
* *The forbidden art* They just study what is illegal, unethical or too dangerous for other schools.

---


*Bruthians a elemental being freed of the chains of finiteness
