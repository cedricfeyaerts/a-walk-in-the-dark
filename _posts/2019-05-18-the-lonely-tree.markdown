---
layout: post
title:  "The lonely tree"
date:   2019-05-18 15:32:14 -0300
categories: story
---

Deep in the forest, far from any path, far from anybody living far from any path, there stood a tree. A very old tree. The oldest of the forest. Older than the word “forest”. Older than the dirt it grows upon. Older than the first fist full of dirt. Older than the sun that shines on its leaves. Older than the star that fathered the sun. Older than oldness itself. Because before it all, there was a tree.

Because of its age, the tree knew everything that ever needed to be known. The tree knew every word of every language. It knew the true name of everything. But it was a tree, and it didn’t speak.
Its bark was dark like a moonless night. It was higher than most trees, but larger still. Its shape twisted and hunched. Its branch was rotting and seeping black sap.

The tree was dying. It was dying for a very long time and would be dying for a longer time still. Old trees are wise, but dying trees are wicked. It was the wisest and the most wicked.

From time to time it grew a fruit, a fruit shrivelled and rotted as the rest. As the legend says, would you take a bite, you would make one with the tree and its infinite knowledge. The legend often failed to precise that the tree would also make one with you. And if your mind didn’t collapse, it would drown in the tree’s wicked mind. The tree would then get another pair of eyes.
